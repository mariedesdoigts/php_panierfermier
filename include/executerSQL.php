<?php

function executeRequete($cnx, $sql, $data = null) {

    if ($data == null) {
        
        $result = $cnx->query($sql);
        
    } else {

        $result = $cnx->prepare($sql);

        $result->execute($data);

    }
    return $result;
}
