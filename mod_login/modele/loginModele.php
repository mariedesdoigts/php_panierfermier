<?php
require_once 'include/connexion.php';
require_once 'include/executerSQL.php';

// RECUPERER TOUTES LES DONNES D'UN INDIVIDU EN FONCTION DE SON MAIL
function individuParMail($mail) {
    $sql = "SELECT * FROM individu WHERE MINDIV = ?";
    $cnx = connect();
    $result = executeRequete($cnx, $sql, array($mail));
    return $result;
}

// ENREGISTRER UN NOUVEL INDIVIDU
function enregistrementIndividu($nom,$prenom,$mail,$password){
    $sql = "INSERT INTO individu (NINDIV, PINDIV, MINDIV, MPAIND) VALUES (?, ?, ?, ?)";
    $cnx = connect();
    executeRequete($cnx, $sql, array($nom,$prenom,$mail,$password));
}
