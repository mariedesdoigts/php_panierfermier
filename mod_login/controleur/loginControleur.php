<?php

// Chargement des fonctions de contrôle de validité des champs
require_once('include/utilitaires.php');

function deconnexion() {
    session_destroy();
    header('Location: index.php');
}

// Page CONNEXION
function connexion($parameters = null) {

    require_once 'mod_login/vue/connexionVue.php';
}

// Contrôle de validite de connexion et initialisation de $_SESSION
function controleConnexion() {

    $mail_tmp = strtolower($_POST['mail']);
    if (!validiteMail($mail_tmp)) {
        
    }

    $password_tmp = $_POST['password'];
    if (!validitePassword($password_tmp)) {
        
    }
    
    $password_tmp = cryptage($password_tmp);
    
    require_once 'mod_login/modele/loginModele.php';
    $data = individuParMail($mail_tmp);

    if ($data->rowCount() == 1) {
        $row = $data->fetch(PDO::FETCH_ASSOC);

        // SI MDP CORRECT
        if ($row['MPAIND'] == $password_tmp) {
            //Enregistrement de la session
            $_SESSION['ID'] = $row['IINDIV'];
            $_SESSION['nom'] = $row['NINDIV'];
            $_SESSION['prenom'] = $row['PINDIV'];
            $_SESSION['mail'] = $row['MINDIV'];

            if (isset($_POST['redirection'])) {
                $redirection = $_POST['redirection'];
                header('Location: index.php?gestion=' . $redirection);
            } else {
                header('Location: index.php');
            }
        } else {
            echo 'Mot de passe ou Mail incorrect';
            require_once 'mod_login/vue/connexionVue.php';
        }
    } else {

        echo 'Mot de passe ou Mail incorrect';
        require_once 'mod_login/vue/connexionVue.php';
    }
}

// Page INSCRIPTION
function inscription($parameters = null) {

    require_once 'mod_login/vue/inscriptionVue.php';
}

// Contrôle de validite de l'inscription
function controleInscription() {

    $valide = true;

    // verification nom
    $nom_tmp = strtolower($_POST['nom']);
    $nomValide = (validiteNomPrenom($nom_tmp));
    if ($valide == true) {
        $valide = $nomValide;
    }
    // verification prenom
    $prenom_tmp = strtolower($_POST['prenom']);
    $prenomValide = (validiteNomPrenom($prenom_tmp));
    if ($valide == true) {
        $valide = $prenomValide;
    }

    // verification mail
    $mail_tmp = strtolower($_POST['mail']);
    $mailValide = (validiteMail($mail_tmp));
    if ($valide == true) {
        $valide = $mailValide;
    }

    // verification password
    $password_tmp = $_POST['password'];
    $passwordValide = (validitePassword($password_tmp));
    if ($valide == true) {
        $valide = $passwordValide;
    }

    // verification passwordtest
    $passwordtest_tmp = $_POST['passwordtest'];
    $passwordtestValide = ($password_tmp == $passwordtest_tmp);
    if ($valide == true) {
        $valide = $passwordtestValide;
    }


    //SI ERREUR (Message erreur dans inscriptionVue.php)
    if ($valide == false) {
        afficheErreur('CHAMP(S) INVALIDE');
        require_once 'mod_login/vue/inscriptionVue.php';

        //SI PAS D'ERREUR
    } else {

        //VERIFICATION QUE LE MAIL N'EST PAS DEJA ENREGISTRE
        require_once 'mod_login/modele/loginModele.php';
        $resultat = individuParMail($mail_tmp);

        //SI LE MAIL N'EST PAS DEJA ENREGISTRE
        if ($resultat->rowCount() == 0) {
            $gauche = "br25&x%";
            $droite = "rj!@";
            $password_tmp = hash('ripemd128', "$gauche$password_tmp$droite");

            enregistrementIndividu($nom_tmp, $prenom_tmp, $mail_tmp, $password_tmp);

            echo 'Votre inscription a été validée. <br>'
            . 'Vous pouvez maintenant vous connecter.';
            require_once 'mod_login/vue/connexionVue.php';

            //SI LE MAIL EST DEJA ENREGISTRE
        } else {
            echo 'Cette adresse mail est déjà enregistrée <br>';
            require_once 'mod_login/vue/inscriptionVue.php';
        }
    }
}

