<?php
require_once 'include/connexion.php';
require_once 'include/executerSQL.php';

function individuParMail($mail) {
    $sql = "SELECT * FROM individu WHERE MINDIV = ?";
    $cnx = connect();
    $result = executeRequete($cnx, $sql, array($mail));
    return $result;
}

function enregistrementIndividu($nom,$prenom,$mail,$password){
    $sql = "INSERT INTO individu (NINDIV, PINDIV, MINDIV, MPAIND) VALUES (?, ?, ?, ?)";
    $cnx = connect();
    executeRequete($cnx, $sql, array($nom,$prenom,$mail,$password));
}

function majIndividu($nom, $prenom, $mail, $id){
    $sql = "UPDATE individu SET NINDIV = ?, PINDIV = ?, MINDIV = ? WHERE IINDIV = ?";
    $cnx = connect();
    executeRequete($cnx, $sql, [$nom, $prenom, $mail, $id]);
}

function passwordById($Id){
    $sql = "SELECT MPAIND FROM INDIVIDU WHERE IINDIV = ?";
    $cnx = connect();
    $result = executeRequete($cnx, $sql, [$Id]);
    return $result;
}

function updatePasswordById($password, $Id){
    $sql = "UPDATE INDIVIDU SET MPAIND = ? WHERE IINDIV = ?";
    $cnx = connect();
    executeRequete($cnx, $sql, [$password, $Id]);
}
